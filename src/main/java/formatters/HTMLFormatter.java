package formatters;
import com.galvanize.Booking;

public class HTMLFormatter implements Formatter{
    @Override
    public String format(Booking booking) {
        String ln1 = "<dl>\n"+
        "  <dt>Type</dt><dd>"+booking.getRoomType()+"</dd>\n" +
        "  <dt>Room Number</dt><dd>"+booking.getRoomNumber()+"</dd>\n" +
        "  <dt>Start Time</dt><dd>"+booking.getStartTime()+"</dd>\n" +
        "  <dt>End Time</dt><dd>"+booking.getStopTime()+"</dd>\n" +
        "</dl>";
        return ln1;
    }
}
