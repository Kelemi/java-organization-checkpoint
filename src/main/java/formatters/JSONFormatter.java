package formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter{
    /*
    {
  "type": "Suite",
  "roomNumber": 111,
  "startTime": "08:30am",
  "endTime": "11:00am"
}
     */
    @Override
    public String format(Booking booking) {
        return String.format("{\n  \"type\": \"%s\",\n  \"roomNumber\": %s,\n  \"startTime\": \"%s\",\n  \"endTime\": \"%s\"\n} ", booking.getRoomType(), booking.getRoomNumber(), booking.getStartTime(), booking.getStopTime());
    }
}
