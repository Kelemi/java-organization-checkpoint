package com.galvanize;

import formatters.CSVFormatter;
import formatters.Formatter;
import formatters.HTMLFormatter;
import formatters.JSONFormatter;

public class Application {
    public static Formatter getFormatter(String input){
        switch(input.toLowerCase()){
            case "html": return new HTMLFormatter();
            case "json": return new JSONFormatter();
            case "csv": return new CSVFormatter();
        }
        return null;
    }
    public static void main(String[] args) {
        Formatter formatter = getFormatter(args[1]);
        Booking booking = Booking.parse(args[0]);
        System.out.println(formatter.format(booking));
    }
}