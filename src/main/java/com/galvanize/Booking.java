package com.galvanize;

public class Booking {
    enum RoomType {R,S,A,C}
    String roomType;
    String roomNumber;
    String startTime;
    String stopTime;
    static public Booking parse(String details){
        //a14-8:30am-9:00am
        String[] myDetails = details.split("");
        String firstChar = myDetails[0];
        myDetails = details.substring(1).split("-");
        return new Booking(RoomType.valueOf(firstChar.toUpperCase()),myDetails[0],myDetails[1],myDetails[2]);
    }
    Booking(RoomType rType,String rNum, String beginTime, String endTime){
        switch (rType){
            case R:
                roomType = "Conference Room";
                break;
            case A:
                roomType = "Auditorium";
                break;
            case C:
                roomType = "Classroom";
                break;
            case S:
                roomType = "Suite";
                break;
        }
        roomNumber = rNum;
        startTime = beginTime;
        stopTime = endTime;
    }
    public String getRoomType(){
        return roomType;
    }
    public String getRoomNumber(){
        return roomNumber;
    }
    public String getStartTime(){
        return startTime;
    }
    public String getStopTime(){
        return stopTime;
    }
}
