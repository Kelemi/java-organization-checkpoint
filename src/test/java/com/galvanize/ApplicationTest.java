package com.galvanize;

import formatters.Formatter;
import formatters.HTMLFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.text.html.HTML;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.Normalizer;

import static com.galvanize.Application.getFormatter;
import static com.galvanize.Application.main;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void aTest() {
        // Write your tests here
        /*
                String[] args = {bookingString, "html"};
        Application.invoke("main", new Object[]{args});

        assertEquals(expected, outContent.toString().trim(), "HTML output does not match");
         */
        // outContent.toString() will give you what your code printed to System.out
        //
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Auditorium</dd>\n" +
                "  <dt>Room Number</dt><dd>13</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30AM</dd>\n" +
                "  <dt>End Time</dt><dd>10:30AM</dd>\n" +
                "</dl>";
        Booking trip = Booking.parse("a13-08:30AM-10:30AM");
        Formatter actual = getFormatter("html");
        assertEquals(expected,actual.format(trip));
    }
    @Test
    public void aTestCSV(){
        String expected = "type,room number,start time,end time\n" +
                "Auditorium,111,08:30AM,11:00AM";
        Booking trip = Booking.parse("a111-08:30AM-11:00AM");
        Formatter actual = getFormatter("csv");
        assertEquals(expected,actual.format(trip));
        assertTrue(true);
    }
    @Test
    public void aTestJSON(){
        String expected = String.format("{\n" +
                "  \"type\": \"Auditorium\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30AM\",\n" +
                "  \"endTime\": \"11:00AM\"\n" +
                "} ");
        Booking trip = Booking.parse("a111-08:30AM-11:00AM");
        Formatter actual = getFormatter("json");
        assertEquals(expected,actual.format(trip));
    }
    @Test
    public void testMain(){
        String expected = "type,room number,start time,end time\n" +
                "Auditorium,111,08:30AM,11:00AM\n";
        main(new String[]{"a111-08:30AM-11:00AM", "csv"});
        String actual = outContent.toString();
        assertEquals(expected, actual);
    }

}