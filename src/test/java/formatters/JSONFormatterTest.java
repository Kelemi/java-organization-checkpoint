package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static com.galvanize.Application.getFormatter;
import static org.junit.jupiter.api.Assertions.*;

class JSONFormatterTest {

    @Test
    void format() {
        String expected = String.format("{\n" +
                "  \"type\": \"Auditorium\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30AM\",\n" +
                "  \"endTime\": \"11:00AM\"\n" +
                "} ");
        Booking trip = Booking.parse("a111-08:30AM-11:00AM");
        Formatter actual = getFormatter("json");
        assertEquals(expected,actual.format(trip));
    }
}