package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static com.galvanize.Application.getFormatter;
import static org.junit.jupiter.api.Assertions.*;

class HTMLFormatterTest {

    @Test
    void format() {
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Auditorium</dd>\n" +
                "  <dt>Room Number</dt><dd>13</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30AM</dd>\n" +
                "  <dt>End Time</dt><dd>10:30AM</dd>\n" +
                "</dl>";
        Booking trip = Booking.parse("a13-08:30AM-10:30AM");
        Formatter actual = getFormatter("html");
        assertEquals(expected,actual.format(trip));
    }
}