package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static com.galvanize.Application.getFormatter;
import static org.junit.jupiter.api.Assertions.*;

class CSVFormatterTest {
    @Test
    void format() {
        String expected = "type,room number,start time,end time\n" +
                "Auditorium,111,08:30AM,11:00AM";
        Booking trip = Booking.parse("a111-08:30AM-11:00AM");
        Formatter actual = getFormatter("csv");
        assertEquals(expected,actual.format(trip));
    }


}